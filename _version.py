import git

repo = git.Repo(search_parent_directories=True)

__version__ = repo.head.object.hexsha
